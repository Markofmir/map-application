<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function index(){
    	return Location::all();
    }

    public function addLocation(Request $request){

    	DB::table('locations')->insert([
        	'name' => $request->name,
        	'address' => $request->address,
        	'city' => $request->city,
        	'hours' => $request->hours,
        	'latitude' => $request->latitude,
        	'longitude' => $request->longitude,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            
        ]);

        return view('app');

    }
}
