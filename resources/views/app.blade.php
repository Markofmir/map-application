<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Map Application</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:2rem}
        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Locations</h2>
                <div class="map" id="app">
                    <gmap-map
                        :center="mapCenter"
                        :zoom="10"
                        map-type-id="terrain"
                        style="width: 100%; height:420px;"
                        @click="handleMapClick"
                    >
                        <gmap-info-window
                            :options="infoWindowOptions"
                            :position="infoWindowPosition"
                            :opened="infoWindowOpened"
                            
                        ><!--Use thos to get map clicking working again @closeclick="handleInfoWindowClose"-->
                            <div class="info-window">
                                <h2 v-text="activeLocation.name"></h2>
                                <h5 v-text="'Hours: ' + activeLocation.hours"></h5>
                                <p v-text="activeLocation.address"></p>
                                <p v-text="activeLocation.city"></p>
                            </div>
                        </gmap-info-window>
                        <gmap-marker
                            v-for="(r, index) in locations"
                            :key="r.id"
                            :position="getPosition(r)"
                            :clickable="true"
                            :draggable="false"
                            @click="handleMarkerClicked(r)"
                        >
                        </gmap-marker>


                    </gmap-map>
                </div>
                <div>
                    <h2>Add a Location</h2>
                    <form action="/" method="POST">
                        @csrf <!--Maybe not needed buuuuuut who knows-->
                        <input type="text" name="name" placeholder="Enter Name"/>
                        <input type="text" name="address" placeholder="Enter Address"/>
                        <input type="text" name="city" placeholder="Enter City"/>
                        <input type="text" name="hours" placeholder="Enter Hours"/>
                        <input type="text" name="latitude" placeholder="Enter Latitude"/>
                        <input type="text" name="longitude" placeholder="Enter Longitude"/>
                        <button type="submit">Add Location</button>
                    </form>
                </div>
            </div> 
        </div>

        <script src="{{ mix('js/app.js') }}"></script>

    </body>
</html>
